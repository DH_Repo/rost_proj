from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from account.models import Account, Food


class HomeView(TemplateView):
    template_name = 'home.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['account'] = Account.objects.all()
        print(Account.objects.all())
        obj = Account.objects.get(name='이동현')

        for o in obj.account_food.all():
            print(o.name)

        obj1 = Food.objects.get(name='피자')
        print(obj1.account.name)


        return context