from django.db import models

# 계정 정
class Account(models.Model):
    name = models.CharField(max_length=30, null=True, blank=True)
    birthDate = models.DateField(null=True, blank=True)
    job = models.CharField(max_length=30, null=True, blank=True)
    card = models.ManyToManyField(
        'Card',
        through='AccountCard',
        related_name='card_account', blank=True
    )
    class Meta:
        db_table = 'account'

# account 와 card의 다대다를 표현하는 관계 테이블
class AccountCard(models.Model):
    card = models.ForeignKey('Card', null=True, blank=True, on_delete=models.CASCADE, related_name='card_account_card')
    account = models.ForeignKey('Account', null=True, blank=True, on_delete=models.CASCADE, related_name='account_account_card')
    class Meta:
        db_table = 'account_card'

# 카드 정보. account 와 다대다 관계.
class Card(models.Model):
    name = models.CharField(max_length=30, null=True, blank=True)
    account = models.ManyToManyField(
        'Account',
        through='AccountCard',
        related_name='account_card', blank=True
    )
    class Meta:
        db_table = 'card'

# 음식 정보 테이블. account와 일대다를 나타냄. 다에 해당하는 테이블이 일에 해당하는 테이블을 참조한다.
class Food(models.Model):
    name = models.CharField(max_length=30, null=True, blank=True)
    account = models.ForeignKey('Account', null=True, blank=True, on_delete=models.CASCADE,
                                related_name='account_food')
    class Meta:
        db_table = 'food'







