
from rest_framework import routers

from games import views

router = routers.DefaultRouter()
router.register(r'songs', views.GETSONGS)