from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from games.models import ArtGames
from games.serializers import ArtGameSerializer


class ArtGames(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = ArtGames.objects.all()
    serializer_class = ArtGameSerializer

    def list(self, request):
        pass