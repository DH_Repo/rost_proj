from rest_framework import serializers

from games.models import ArtGames


class ArtGameSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ArtGames
        fields = '__all__'