from django.db import models

# Create your models here.
class ArtGames(models.Model):
    GamesFieldCode = models.CharField(max_length=20, null=True, blank=True)
    GamesName = models.CharField(max_length=50, null=True, blank=True)
    GamesStartDateTime = models.DateTimeField(null=True, blank=True)
    GamesEndDateTime = models.DateTimeField(null=True, blank=True)
    Place = models.CharField(max_length=300, null=True, blank=True)
    MberPost = models.CharField(max_length=20, null=True, blank=True)
    MberDefaultAddress = models.CharField(max_length=100, null=True, blank=True)
    MberDetailAddress = models.CharField(max_length=150, null=True, blank=True)
    Latitude = models.CharField(max_length=150, null=True, blank=True)
    Longitude = models.CharField(max_length=150, null=True, blank=True)
    Auspice = models.CharField(max_length=50, null=True, blank=True)
    ManageCode = models.CharField(max_length=10, null=True, blank=True)
    WorkExplain = models.TextField(null=True, blank=True)
    ParticipationPersonnel = models.IntegerField(null=True, blank=True)
    ParticipantQualification = models.TextField(null=True, blank=True)
    RecruitStartDay = models.DateField(null=True, blank=True)
    RecruitCloseDay = models.DateField(null=True, blank=True)
    MatchTableByLotDate = models.DateField(null=True, blank=True)
    PointsPerGame = models.IntegerField(null=True, blank=True)
    SetCnt = models.IntegerField(null=True, blank=True)
    DoubleGameBallCode = models.CharField(max_length=10, null=True, blank=True)
    OfficialBallCode = models.CharField(max_length=10, null=True, blank=True)
    GameRule = models.TextField(null=True, blank=True)
    DressCode = models.TextField(null=True, blank=True)
    ParticipationFee = models.TextField(null=True, blank=True)
    Awards = models.TextField(null=True, blank=True)
    GamesSouvenir = models.TextField(null=True, blank=True)
    Insurance = models.TextField(null=True, blank=True)
    InsuranceShowYn = models.BooleanField(null=True, blank=True)
    PrizeByLot = models.TextField(null=True, blank=True)
    Etc = models.TextField(null=True, blank=True)
    PlayerSectCode = models.CharField(max_length=10, null=True, blank=True)
    GamesGradeCode = models.CharField(max_length=10, null=True, blank=True)
    MatchTableGradeCode = models.CharField(max_length=10, null=True, blank=True)
    CriteriaPoint = models.IntegerField(null=True, blank=True)
    GamesStatusCode = models.CharField(max_length=10, null=True, blank=True)
    Owner = models.CharField(max_length=50, null=True, blank=True)
    Contact = models.CharField(max_length=13, null=True, blank=True)
    Email = models.EmailField(max_length=20, null=True, blank=True)
    ConfirmStatus = models.BooleanField(null=True, blank=True)
    ConfirmDay = models.DateTimeField(null=True, blank=True)
    CreateDate = models.DateTimeField(null=True, blank=True)
    UpdateDate = models.DateTimeField(null=True, blank=True)
    CreateUser = models.ForeignKey('member.ArtMber', blank=True, on_delete=models.SET_NULL,
                                   related_name='create_user_games', null=True)
    UpdateUser = models.ForeignKey('member.ArtMber', blank=True, on_delete=models.SET_NULL,
                                   related_name='update_user_games', null=True)
    class Meta:
        db_table = 'games_art_games'

class ArtGame(models.Model):
    games = models.ForeignKey('ArtGames', blank=True, on_delete=models.CASCADE,
                              related_name='games_artgame')
    gender = models.CharField(max_length=5, null=True, blank=True)
    level = models.CharField(max_length=5, null=True, blank=True)
    memb_participation = models.ManyToManyField('member.ArtMber', through='ArtGameParticipationMber'
                                                ,related_name='memb_participation_art_game',
                                                blank=True)
    class Meta:
        db_table = 'games_art_game'


class ArtGameParticipationMber(models.Model):
    game = models.ForeignKey('ArtGame', blank=True, on_delete=models.CASCADE,
                                   related_name='game_art_game_participation')

    participation = models.ForeignKey('member.ArtMber', blank=True, on_delete=models.CASCADE,
                                   related_name='participation_art_game_participation')
    confirm = models.BooleanField(null=True, blank=True)
    class Meta:
        db_table = 'games_mtm_game_member'
